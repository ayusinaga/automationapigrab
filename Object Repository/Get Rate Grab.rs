<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get Rate Grab</name>
   <tag></tag>
   <elementGuidId>f170c27b-0bb7-4829-8975-5dd58fb97675</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;packages\&quot;: [\n    {\n      \&quot;name\&quot;: \&quot;Baju\&quot;,\n      \&quot;description\&quot;: \&quot;Baju\&quot;,\n      \&quot;quantity\&quot;: 1,\n      \&quot;price\&quot;: 10000,\n      \&quot;dimensions\&quot;: {\n        \&quot;height\&quot;: 1,\n        \&quot;width\&quot;: 1,\n        \&quot;depth\&quot;: 1,\n        \&quot;weight\&quot;: 1\n      }\n    }\n  ],\n  \&quot;origin\&quot;: {\n    \&quot;address\&quot;: \&quot;jl.hayam wuruk\&quot;,\n    \&quot;keywords\&quot;: \&quot;hayam wuruk\&quot;,\n    \&quot;coordinates\&quot;: {\n      \&quot;latitude\&quot;: -6.166892,\n      \&quot;longitude\&quot;: 106.820899\n    },\n    \&quot;extra\&quot;: {}\n  },\n  \&quot;destination\&quot;: {\n    \&quot;address\&quot;: \&quot;Jl.Gadjah Mada\&quot;,\n    \&quot;keywords\&quot;: \&quot;Gadjah Mada\&quot;,\n    \&quot;coordinates\&quot;: {\n      \&quot;latitude\&quot;: -6.14104,\n      \&quot;longitude\&quot;: 106.814285\n    },\n    \&quot;extra\&quot;: {}\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>safe_id</name>
      <type>Main</type>
      <value>09c9c534-b568-4b34-ba41-c164f44e2d7a</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>secret</name>
      <type>Main</type>
      <value>SPiFUvb9gyXFeysb</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://159.89.194.25:5000/grab/deliveries/quotes?=</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
