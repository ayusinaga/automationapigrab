<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Create Order Grab</name>
   <tag></tag>
   <elementGuidId>c5c376ee-80cf-4e3b-ab2e-69fb5b5626cf</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\&quot;merchantOrderID\&quot;: \&quot;1E71280\&quot;,\n\&quot;serviceType\&quot;: \&quot;SAME_DAY\&quot;,\n\&quot;packages\&quot;: [\n{\n\&quot;name\&quot;: \&quot;sepatu\&quot;,\n\&quot;description\&quot;: \&quot;sepatu\&quot;,\n\&quot;quantity\&quot;: 1,\n\&quot;price\&quot;: 200000,\n\&quot;dimensions\&quot;: {\n\&quot;height\&quot;: 1,\n\&quot;width\&quot;: 1,\n\&quot;depth\&quot;: 1,\n\&quot;weight\&quot;: 1\n}\n}\n],\n\&quot;cashOnDelivery\&quot;: {\n\&quot;amount\&quot;: 0\n},\n\&quot;sender\&quot;: {\n\&quot;firstName\&quot;: \&quot;shipper gm\&quot;,\n\&quot;lastName\&quot;: \&quot;shipper gajah mada\&quot;,\n\&quot;title\&quot;: \&quot;Mr/Mrs\&quot;,\n\&quot;companyName\&quot;: \&quot;Shipper\&quot;,\n\&quot;email\&quot;: \&quot;order@shipper.id\&quot;,\n\&quot;phone\&quot;: \&quot;6281808515863\&quot;,\n\&quot;smsEnabled\&quot;: true,\n\&quot;instruction\&quot;: \&quot;none\&quot;\n},\n\&quot;recipient\&quot;: {\n\&quot;firstName\&quot;: \&quot;shipper hm\&quot;,\n\&quot;lastName\&quot;: \&quot;shipper hayam wuruk\&quot;,\n\&quot;title\&quot;: \&quot;Mr/Mrs\&quot;,\n\&quot;companyName\&quot;: \&quot;shipper\&quot;,\n\&quot;email\&quot;: \&quot;order@shipper.id\&quot;,\n\&quot;phone\&quot;: \&quot;6281322276873\&quot;,\n\&quot;smsEnabled\&quot;: true,\n\&quot;instruction\&quot;: \&quot;none\&quot;\n},\n\&quot;origin\&quot;: {\n\&quot;address\&quot;: \&quot;gajah mada2\&quot;,\n\&quot;keywords\&quot;: \&quot;pintu harmoni\&quot;,\n\&quot;coordinates\&quot;: {\n\&quot;latitude\&quot;: -6.166892,\n\&quot;longitude\&quot;: 106.820899\n}\n},\n\&quot;destination\&quot;: {\n\&quot;address\&quot;: \&quot;hayam wuruk\&quot;,\n\&quot;keywords\&quot;: \&quot;glodok ltc\&quot;,\n\&quot;coordinates\&quot;: {\n\&quot;latitude\&quot;: -6.14104,\n\&quot;longitude\&quot;: 106.814285\n}\n}\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>safe_id</name>
      <type>Main</type>
      <value>09c9c534-b568-4b34-ba41-c164f44e2d7a</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>secret</name>
      <type>Main</type>
      <value>SPiFUvb9gyXFeysb</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://159.89.194.25:5000/grab/deliveries</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
